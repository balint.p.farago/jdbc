public class Main {

    public static void main(String[] args) {
        DB db = new DB();
        Person person = new Person("Andras", "Gyurcsek", "1983-01-01", "Andris");
        Person person2 = new Person("Gergo", "Varga", "1994-01-01", "Gery");
        //db.addPerson("Zsolt", "Himmer", "1987-01-01", "Zsoltika");
        //db.addPersonViaPojo(person);
        //db.addPersonWithPreparedStatement(person2);
        db.showAllPerson();
        //db.showPersonMetaData();
        /*for (int i = 0; i < db.getAllPersons().size(); i++){
            System.out.println(db.getAllPersons().get(i));
        }*/
        db.updateBrthdayOnePerson(1);
        db.showAllPerson();
    }
}
