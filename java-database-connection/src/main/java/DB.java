
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DB {

    final String JDBC_DRIVER = "org.h2.Driver";  //unused
    final String URL = "jdbc:h2:D:\\Programming\\gitlab\\balint\\javaprojects\\JavaDatabaseConnection\\java-database-connection\\src\\main\\resources\\person";
    final String USERNAME = "sa";
    final String PASSWORD = "";

    Connection connection = null;
    Statement createStatement = null;
    DatabaseMetaData dbmd = null;

    public DB() {

        try {
            DriverManager.registerDriver(new org.h2.Driver());
            System.out.println("db connection created");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);  // getconnection static method gives back a Connection
            System.out.println("connection initialization works");

        } catch (SQLException e) {
            System.out.println("connection initialization does not work");
            e.printStackTrace();
        }

        if (connection != null) {  //If there is connection
            try {
                createStatement = connection.createStatement();  // we get a Statement from createStaement method
                System.out.println("createStatement works");
            } catch (SQLException e) {
                System.out.println("createStatement does not work");
                e.printStackTrace();
            }
        }

        try {
            dbmd = connection.getMetaData();  //returns database  metadata object
            /*//System.out.println("tables " +createStatement.execute("SHOW TABLES"));
            ResultSet rs = createStatement.getResultSet();
            while (rs.next()){
                System.out.println(rs.getRow());
            }*/

        } catch (SQLException e) {
            System.out.println(e);
            e.printStackTrace();
        }

        try {
            ResultSet rs1 = dbmd.getTables(null, "APP", "person", null);  //database metadata has a getTable method wich returns a resultSet
            System.out.println(rs1);
            // If I'd need to make sure the table is created before running the APP, I would use this to create a table if I no had before
            /*if (!rs1.next()) {  // If the resuktset (table) has no element the table will be created
                createStatement.execute("CREATE TABLE `PERSON` (\n" +

                       "  `ID` int(11) NOT NULL AUTO_INCREMENT,\n" +

                       "  `FIRSTNAME` varchar(255) NOT NULL COMMENT 'Persons first name',\n" +

                       "  `LASTNAME` varchar(255) COMMENT 'Persons last name',\n" +

                       "  `BIRTHDAY` date NOT NULL COMMENT 'Birthday',\n" +

                       "  `NICKNAME` varchar(255) NOT NULL,\n" +

                       "  PRIMARY KEY (`ID`),\n" +

                       "  UNIQUE(`NICKNAME`)\n" +

                       ");\n");
           }*/
        } catch (SQLException e) {
            System.out.println("You have already such table!");
            e.printStackTrace();
        }
    }

    //add person to DB
    public void addPerson(String firstName, String lastName, String birthday, String nickName){
        String sql = "INSERT INTO PERSON (`FIRSTNAME`, `LASTNAME`, `BIRTHDAY`, `NICKNAME`) VALUES ('" + firstName + "', '" + lastName + "', '" + birthday +"', '" + nickName +"');";
        try {
            System.out.println(sql);
            createStatement.execute(sql);
            System.out.println("createStatement.execute works");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("createStatement.execute does not work");
        }
    }

    public void addPersonViaPojo (Person person){
        String sql = "INSERT INTO PERSON (`FIRSTNAME`, `LASTNAME`, `BIRTHDAY`, `NICKNAME`) VALUES ('" + person.getFirstName() + "', '" + person.getLastName() + "', '" + person.getBirthday() +"', '" + person.getNickName() +"');";
        try {
            System.out.println(sql);
            createStatement.execute(sql);
            System.out.println("createStatement.execute works");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("createStatement.execute does not work");
        }
    }

    public void addPersonWithPreparedStatement (Person person){
        String sql = "INSERT INTO PERSON (`FIRSTNAME`, `LASTNAME`, `BIRTHDAY`, `NICKNAME`) VALUES (?,?,?,?);";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, person.getFirstName());
            preparedStatement.setString(2, person.getLastName());
            preparedStatement.setString(3, person.getBirthday());
            preparedStatement.setString(4, person.getNickName());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void showAllPerson(){
        String sql = "SELECT * FROM PERSON;";
        try {
            ResultSet resultSet = createStatement.executeQuery(sql);  // resultset = next row

            while (resultSet.next()){
                int id = resultSet.getInt("id");
                String firstName = resultSet.getString("FIRSTNAME");
                String lastName = resultSet.getString("LASTNAME");
                String birthday = resultSet.getString("BIRTHDAY");
                String nickName = resultSet.getString("NICKNAME");

                System.out.println(firstName + " " + lastName + " " + birthday + " " + nickName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void showPersonMetaData(){
        String sql = "SELECT * FROM PERSON";
        ResultSet rs;
        ResultSetMetaData rsmd;

        try {
            rs = createStatement.executeQuery(sql);
            rsmd = rs.getMetaData();

            int columnCount = rsmd.getColumnCount();

            for (int i = 1; i <= columnCount; i++){
                System.out.print(rsmd.getColumnName(i) + " | ");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //create a method what returns all Person from DB to a list
    List<Person> getAllPersons(){
        String sql = "SELECT * FROM PERSON;";
        List<Person> personList = new ArrayList<>();

        try {
            ResultSet resultSet = createStatement.executeQuery(sql);  // resultset = next row

            while (resultSet.next()){
                String firstName = resultSet.getString("FIRSTNAME");
                String lastName = resultSet.getString("LASTNAME");
                String birthday = resultSet.getString("BIRTHDAY");
                String nickName = resultSet.getString("NICKNAME");

                personList.add(new Person(firstName, lastName, birthday, nickName));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return personList;
    }

    //update one person birthday birthday + 2 years
    public void updateBrthdayOnePerson(int id){

        String birthday = null;
        String sql = "SELECT BIRTHDAY FROM PERSON WHERE ID = " + id;
        try {
            ResultSet resultSet = createStatement.executeQuery(sql);
            while (resultSet.next()) {
                birthday = resultSet.getString("BIRTHDAY");
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        String birthdayWithoutYear = birthday.substring(4);
        String incrementedBirthdayYear = (Integer.parseInt(birthday.substring(0,4)) + 2) + "";
        String sql2 = "UPDATE PERSON SET BIRTHDAY = ? WHERE ID = " + id;
        birthday = incrementedBirthdayYear + birthdayWithoutYear;
        System.out.println(birthday);

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql2);
            preparedStatement.setString(1, birthday);
            preparedStatement.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    //delete one person
}

